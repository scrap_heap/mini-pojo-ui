package ink.xieyu.annotation;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
@CommonBeanBuilderAnnotation
public @interface Service {
    /**
     * bean名称
     *
     * @return
     */
    String value() default "";
}