package ink.xieyu.annotation;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface CommonBeanBuilderAnnotation {
    /**
     * bean名称
     *
     * @return
     */
    String value() default "";

}
