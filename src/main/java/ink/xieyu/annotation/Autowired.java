package ink.xieyu.annotation;

import java.lang.annotation.*;

/**
 * 模仿spring自动装配
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
public @interface Autowired {
    /**
     * 是否必须 默认必须
     */
    boolean require() default true;
}
