package ink.xieyu.ex;

/**
 * bean生成异常
 */
public class BeanBuilderException extends Exception {
    public BeanBuilderException() {
    }

    public BeanBuilderException(String message, Throwable cause) {
        super(message, cause);
    }

    public BeanBuilderException(String message) {
        super(message);
    }
}
