package ink.xieyu.controller;

import ink.xieyu.annotation.Autowired;
import ink.xieyu.annotation.Controller;
import ink.xieyu.service.HelloService;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

@Controller
public class HelloController {
    String a;
    String b;
    String c;
    private HelloController() {
        System.err.println("hello无参构造");
    }
    @Autowired
    private HelloService helloService;
    public HelloController(String a) {
        this.a = a;
        System.err.println("hello1参构造");

    }

    public HelloController(String a, String b) {
        this.a = a;
        this.b = b;
        System.err.println("hello2参构造");

    }

    public static void main(String[] args) {
        try {
            Constructor<?> constructor =HelloController.class.getConstructor();
            try {
                System.err.println(constructor.newInstance());
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
