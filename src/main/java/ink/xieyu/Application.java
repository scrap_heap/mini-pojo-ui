package ink.xieyu;


import ink.xieyu.ex.BeanBuilderException;
import ink.xieyu.utils.scan.ScanAllClass;

/**
 * 应用启动
 */
public class Application {
    public static void main(String[] args) throws BeanBuilderException {
        new ScanAllClass().start(Application.class);

    }
}


