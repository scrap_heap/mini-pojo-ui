package ink.xieyu.utils.charutils;

public class StringUtil {
    /**
     * 首字母处理成小写
     *
     * @param str 字符串
     * @return 返回
     */
    public static String littleHump(String str) {
        char[] chars = str.toCharArray();
        char[] newChar = new char[chars.length];
        for (int i = 0; i < newChar.length; i++) {
            if (i == 0) {
                newChar[i] = Character.toLowerCase(chars[i]);
                continue;
            }
            newChar[i] = chars[i];
        }
        return new String(newChar);
    }

    /**
     * 判断是否等于空
     *
     * @param str
     * @return
     */
    public static boolean isBlank(String str) {
        return str == null || "".equals(str) || "".equals(str.trim());
    }


}
