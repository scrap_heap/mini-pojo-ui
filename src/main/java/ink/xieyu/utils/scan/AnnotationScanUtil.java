package ink.xieyu.utils.scan;

import ink.xieyu.annotation.CommonBeanBuilderAnnotation;
import ink.xieyu.controller.HelloController;

import java.lang.annotation.*;

/**
 * 注解扫描
 *
 * @author xieyu
 */
public class AnnotationScanUtil {

//    /**
//     * 自动加载
//     */
//    public void autoload (Map<String,Object> map,SeekBeanUtil beanUtil ){
//        map.values().parallelStream().forEach(e->{
//            Arrays.stream(e.getClass().getDeclaredFields()).filter(e->e.)
//
//        });
//    }

    /**
     * 扫描携带CommonBeanBuilderAnnotation注解或者其子类的所有类
     *
     * @param clazz 类
     * @param <T>
     * @return 携带则为true
     */
    public <T> CommonBeanBuilderAnnotation doBeanScan(Class<T> clazz) {
        return doBeanScan(clazz, CommonBeanBuilderAnnotation.class);
//        System.err.println(clazz);
//        for (Annotation annotation : clazz.getAnnotations()) {
//            if (annotation instanceof CommonBeanBuilderAnnotation) {
//                //先判断注解是不是CommonBeanBuilderAnnotation
//                return (CommonBeanBuilderAnnotation) annotation;
//            }
//            if (annotation instanceof Documented || annotation instanceof Retention ||
//                    annotation instanceof Target || annotation instanceof Inherited) {
//                //@Documented
//                //@Retention(RetentionPolicy.RUNTIME)
//                //@Target(ElementType.TYPE)
//                //@Inherited
//                //如果是以上四种注解，则略过
//                return null;
//            }
//            for (Annotation anno : annotation.annotationType().getAnnotations()) {
//                if (anno instanceof CommonBeanBuilderAnnotation) {
//                    return (CommonBeanBuilderAnnotation) anno;
//                }
//                Class<?> cla = anno.annotationType();
//                System.err.println("cla" + cla);
//                if (cla != null) {
//                    CommonBeanBuilderAnnotation commonBeanBuilderAnnotation = doBeanScan(cla);
//                    if (commonBeanBuilderAnnotation != null) {
//                        return commonBeanBuilderAnnotation;
//                    }
//                }
//            }
//        }
//        return null;
    }


    public <T, A> A doBeanScan(Class<T> clazz, Class<A> classBean) {
        for (Annotation annotation : clazz.getAnnotations()) {
            if (annotation.annotationType().equals(classBean) ) {
                //先判断注解是不是CommonBeanBuilderAnnotation
                return (A) annotation;
            }
            if (annotation instanceof Documented || annotation instanceof Retention ||
                    annotation instanceof Target || annotation instanceof Inherited) {
                //@Documented
                //@Retention(RetentionPolicy.RUNTIME)
                //@Target(ElementType.TYPE)
                //@Inherited
                //如果是以上四种注解，则略过
                return null;
            }
            for (Annotation anno : annotation.annotationType().getAnnotations()) {
                if (anno.annotationType().equals(classBean)) {
                    return (A) anno;
                }
                Class<?> cla = anno.annotationType();
                if (cla != null) {
                    A commonBeanBuilderAnnotation = doBeanScan(cla, classBean);
                    if (commonBeanBuilderAnnotation != null) {
                        return commonBeanBuilderAnnotation;
                    }
                }
            }
        }
        return null;
    }

    public static void main(String[] args) {
        System.err.println(new AnnotationScanUtil().doBeanScan(HelloController.class, CommonBeanBuilderAnnotation.class));
    }

}
